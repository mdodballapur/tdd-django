from django.shortcuts import redirect, render
from lists.models import Item, List

# Create your views here.
def home_page(req):
    return render(req, 'home.html')

def view_list(req, list_id):
    list_ = List.objects.get(id=list_id)
    items = Item.objects.filter(list=list_)
    #return render(req, 'list.html', {'items': items })
    return render(req, 'list.html', {'list': list_ })

def add_item_to_list(req, list_id):
    list_ = List.objects.get(id=list_id)
    Item.objects.create(text=req.POST.get('item_text', ''), list=list_)
    return redirect(f'/lists/{list_.id}/')

def new_list(req):
    list_ = List.objects.create()
    Item.objects.create(text=req.POST.get('item_text', ''), list=list_)
    return redirect(f'/lists/{list_.id}/')
