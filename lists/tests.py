from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from lists.views import home_page
from django.template.loader import render_to_string
from lists.models import Item, List

# Create your tests here.
class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_uses_home_template(self):
        res = self.client.get('/')
        self.assertTemplateUsed(res, 'home.html')

class ListAndItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):

        list_ = List()
        list_.save()

        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.list = list_
        first_item.save()

        second_item = Item()
        second_item.text = 'Item the second'
        second_item.list = list_
        second_item.save()

        saved_list = List.objects.first()
        self.assertEqual(saved_list, list_)

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)
        
        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]

        self.assertEqual(first_saved_item.text, 'The first (ever) list item')
        self.assertEqual(first_saved_item.list, list_)
        self.assertEqual(second_saved_item.text, 'Item the second')
        self.assertEqual(second_saved_item.list, list_)

    def test_only_saves_items_when_necessary(self):
        self.client.get('/')
        self.assertEqual(Item.objects.count(), 0)

class NewListTest(TestCase):
    
    def test_can_save_a_POST_request(self):
        res = self.client.post('/lists/new', data={'item_text': 'A new item list'})
        
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item list')

    def test_redirects_after_POST(self):
        res = self.client.post('/lists/new', data={'item_text': 'A new item list'})
        new_list = List.objects.first()
        self.assertRedirects(res, f'/lists/{new_list.id}/')

class ListViewTest(TestCase):
    def test_uses_list_template(self):
        list_ = List.objects.create()
        res = self.client.get(f'/lists/{list_.id}/')
        self.assertTemplateUsed(res, 'list.html')

    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=correct_list)
        Item.objects.create(text='itemey 2', list=correct_list)

        other_list = List.objects.create()
        Item.objects.create(text='other list item 1', list=other_list)
        Item.objects.create(text='other list item 2', list=other_list)

        res = self.client.get(f'/lists/{correct_list.id}/')

        self.assertContains(res, 'itemey 1')
        self.assertContains(res, 'itemey 2')

        self.assertNotContains(res, 'other list item 1')
        self.assertNotContains(res, 'other list item 2')

    def test_passes_correct_list_to_template(self):
        o_list = List.objects.create()
        c_list = List.objects.create()

        res = self.client.get(f'/lists/{c_list.id}/')
        self.assertEqual(res.context['list'], c_list)


class NewItemTest(TestCase):
    def test_can_save_a_POST_request_to_an_existing_list(self):
        o_list = List.objects.create()
        c_list = List.objects.create()

        self.client.post(f'/lists/{c_list.id}/add_item',
                         data = {'item_text': 'A new item for an existing list'})

        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, c_list)


    def test_redirects_to_list_view(self):
        o_list = List.objects.create()
        c_list = List.objects.create()

        res = self.client.post(f'/lists/{c_list.id}/add_item',
                               data = {'item_text': 'A new item for an existing list'})

        self.assertRedirects(res, f'/lists/{c_list.id}/')





        
