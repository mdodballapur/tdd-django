from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
from functools import partial
import time

MAX_WAIT=10

class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_ip(self, row_text):
        start_time = time.time()

        while True:
            try:
                ip_box = self.browser.find_element_by_id('id_new_item')
                self.assertEqual(ip_box.get_attribute('placeholder'), 'Enter a to-do item')
                ip_box.send_keys(row_text)
                time.sleep(2)
                ip_box.send_keys(Keys.ENTER)
                return
            except (AssertionError, WebDriverException) as err:
                if time.time() - start_time > MAX_WAIT:
                    raise err
                time.sleep(0.5)
    
    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()

        while True:
            try:
                table = self.browser.find_element_by_id('id_list_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as err:
                if time.time() - start_time > MAX_WAIT:
                    raise err
                time.sleep(0.5)

    def test_can_start_a_list_and_retrieve_it_later(self):
        print (f"url: {self.live_server_url}")
        self.browser.get(self.live_server_url)
        self.assertIn('To-Do', self.browser.title)

        self.wait_for_row_ip('Buy peacock feathers')
        self.wait_for_row_ip('Use peacock feathers to make a fly')

        self.wait_for_row_in_list_table('1: Buy peacock feathers')
        self.wait_for_row_in_list_table('2: Use peacock feathers to make a fly')

        #self.fail('Finish the test!')
    
        # Make sure lists don't conflict
        self.browser.quit()
        self.browser = webdriver.Firefox()
        print (f"url: {self.live_server_url}")
        self.browser.get(self.live_server_url)
        self.assertIn('To-Do', self.browser.title)


        # Start a new list
        self.wait_for_row_ip('Buy milk')
        time.sleep(15)
        new_list_url = self.browser.current_url
        print (f"url: {self.live_server_url}")
        self.assertRegex(new_list_url, '/lists/.+')
        #self.assertNotEqual(list_url, new_list_url)
        self.wait_for_row_in_list_table('1: Buy milk')

        self.assertRaises(Exception, partial(self.wait_for_row_in_list_table, '1: Buy peacock feathers'))
        self.assertRaises(Exception, partial(self.wait_for_row_in_list_table, '2: Use peacock feathers to make a fly'))

if __name__ == "__main__":
    unittest.main(warnings='ignore')

